# Terraform Module AWS Backup

Module to manage AWS Backup with Terraform.

### Limitations

- This module can only create one optional vault and/or one optional selection by tags and/or one selection by resources.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.30 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.30 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_backup_global_settings.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_global_settings) | resource |
| [aws_backup_plan.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_plan) | resource |
| [aws_backup_region_settings.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_region_settings) | resource |
| [aws_backup_selection.by_resources](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_selection) | resource |
| [aws_backup_selection.by_tags](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_selection) | resource |
| [aws_backup_vault.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_vault) | resource |
| [aws_backup_vault_lock_configuration.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_vault_lock_configuration) | resource |
| [aws_backup_vault_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_vault_policy) | resource |
| [aws_iam_role.this_selection](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.this_selection_backup](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.this_selection_restore](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_kms_alias.this_vault](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.this_vault](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_kms_replica_key.this_vault](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_replica_key) | resource |
| [aws_iam_policy_document.this_selection_assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_global_settings_cross_account_enabled"></a> [global\_settings\_cross\_account\_enabled](#input\_global\_settings\_cross\_account\_enabled) | Whether or not to allow cross account backups. This only works from the root account. | `bool` | `false` | no |
| <a name="input_plan_enabled"></a> [plan\_enabled](#input\_plan\_enabled) | Whether or not to create a plan. | `bool` | `true` | no |
| <a name="input_plan_id"></a> [plan\_id](#input\_plan\_id) | The backup plan ID to be associated with the Backup Selections (by tag or by resources) of resources. Must be specified if `var.plan_enabled` is `false` and any of the Backup Selections are enabled. | `string` | `""` | no |
| <a name="input_plan_name"></a> [plan\_name](#input\_plan\_name) | Name of the backup plan. | `string` | `"backup-plan"` | no |
| <a name="input_plan_rules"></a> [plan\_rules](#input\_plan\_rules) | A rule object that specifies a scheduled task that is used to back up a selection of resources.<br>  * rule\_name                (required, string): An display name for a backup rule.<br>  * schedule                 (required, string): A CRON expression specifying when AWS Backup initiates a backup job. See: https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html?icmpid=docs_console_unmapped<br>  * enable\_continuous\_backup (optional, bool): Enable continuous backups for supported resources.<br>  * start\_window             (optional, string): The amount of time in minutes before beginning a backup.<br>  * completion\_window        (optional, string): The amount of time AWS Backup attempts a backup before canceling the job and returning an error.<br>  * lifecycle                (optional, object): The lifecycle defines when a protected resource is transitioned to cold storage and when it expires. Fields documented below.<br>    * cold\_storage\_after (optional, number): Specifies the number of days after creation that a recovery point is moved to cold storage.<br>    * delete\_after       (optional, number): Specifies the number of days after creation that a recovery point is deleted. Must be 90 days greater than `cold_storage_after`.<br>  * copy\_action              (optional, object): Copy operation settings.<br>    * lifecycle             (optional, object): See `lifecycle`.<br>    * destination\_vault\_arn (optional, string): An Amazon Resource Name (ARN) that uniquely identifies the destination backup vault for the copied backup.<br>  * recovery\_point\_tags        (optional, map(string)): Metadata that you can assign to help organize the resources that you create. Will be merged with `var.tags`. | <pre>list(object({<br>    rule_name                = string<br>    schedule                 = string<br>    enable_continuous_backup = optional(bool)<br>    start_window             = optional(string)<br>    completion_window        = optional(string)<br>    lifecycle = optional(object({<br>      cold_storage_after = optional(number)<br>      delete_after       = optional(number)<br>    }))<br>    recovery_point_tags = optional(map(string))<br>    copy_action = optional(object({<br>      lifecycle = optional(object({<br>        cold_storage_after = optional(number)<br>        delete_after       = optional(number)<br>      }))<br>      destination_vault_arn = string<br>    }))<br>  }))</pre> | `[]` | no |
| <a name="input_plan_tags"></a> [plan\_tags](#input\_plan\_tags) | Tags for the Backup plan. Will be merged with tags. | `map(string)` | `{}` | no |
| <a name="input_region_settings_resource_type_opt_in"></a> [region\_settings\_resource\_type\_opt\_in](#input\_region\_settings\_resource\_type\_opt\_in) | A map of services along with the opt-in preferences for the Region. Once opted-in, services stays opted-in, even if the resources are destroyed. Unmentioned services stays untouched. For values, see: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_region_settings. | `map(bool)` | `{}` | no |
| <a name="input_selection_by_resource_name"></a> [selection\_by\_resource\_name](#input\_selection\_by\_resource\_name) | Name of the selection by resource. Ignored if `var.selection_by_resources_enabled` is `false`. | `string` | `"selection-by-resource"` | no |
| <a name="input_selection_by_resource_resources"></a> [selection\_by\_resource\_resources](#input\_selection\_by\_resource\_resources) | An array of strings that either contain Amazon Resource Names (ARNs) or match patterns of resources to assign to a backup plan. Ignored if `var.selection_by_resources_enabled` is `false`. | `list` | `[]` | no |
| <a name="input_selection_by_resources_enabled"></a> [selection\_by\_resources\_enabled](#input\_selection\_by\_resources\_enabled) | Whether or not to create a Backup Selection by resources. | `bool` | `false` | no |
| <a name="input_selection_by_tag_enabled"></a> [selection\_by\_tag\_enabled](#input\_selection\_by\_tag\_enabled) | Whether or not to create a Backup Selection by tags. | `bool` | `false` | no |
| <a name="input_selection_by_tag_name"></a> [selection\_by\_tag\_name](#input\_selection\_by\_tag\_name) | Name of the selection by tags. Ignored if `var.selection_by_tags_enabled` is `false`. | `string` | `"selection-by-tag"` | no |
| <a name="input_selection_by_tag_tags"></a> [selection\_by\_tag\_tags](#input\_selection\_by\_tag\_tags) | Tag-based conditions used to specify a set of resources to assign to a backup plan.<br>  * type  (required, string): An operation, such as `StringEquals`, that is applied to a key-value pair used to filter resources in a selection.<br>  * key   (required, string): The key in a key-value pair.<br>  * value (required, string): The value in a key-value pair. | <pre>list(object({<br>    type  = string<br>    key   = string<br>    value = string<br>  }))</pre> | `[]` | no |
| <a name="input_selection_role_arn"></a> [selection\_role\_arn](#input\_selection\_role\_arn) | ARN of the IAM role that AWS Backup uses to authenticate when restoring and backing up the target resource. Must be specified if `selection_role_enabled` is `false` and any Backup Selection is enabled. | `any` | `null` | no |
| <a name="input_selection_role_backup_policy_arn"></a> [selection\_role\_backup\_policy\_arn](#input\_selection\_role\_backup\_policy\_arn) | ARN of the policy to attach to the Backup Selection role that AWS Backup uses to authenticate when backing up the target resources. Defaults to the service policy. | `string` | `"arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"` | no |
| <a name="input_selection_role_enabled"></a> [selection\_role\_enabled](#input\_selection\_role\_enabled) | Whether or not to create a role that AWS Backup uses to authenticate when restoring and backing up the target resources. Ignored if any selection is `false`. | `bool` | `true` | no |
| <a name="input_selection_role_name"></a> [selection\_role\_name](#input\_selection\_role\_name) | Name of the IAM role that AWS Backup uses to authenticate when restoring and backing up the target resources. Ignored if `selection_role_enabled` is `false`. | `any` | `null` | no |
| <a name="input_selection_role_restore_policy_arn"></a> [selection\_role\_restore\_policy\_arn](#input\_selection\_role\_restore\_policy\_arn) | ARN of the policy to attach to the Backup Selection role that AWS Backup uses to authenticate when restoring the target resources. Defaults to the service policy. | `string` | `"arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be shared among all resources of this module. | `map(string)` | `{}` | no |
| <a name="input_vault_enabled"></a> [vault\_enabled](#input\_vault\_enabled) | Whether or not to create a vault for AWS Backup. | `bool` | `true` | no |
| <a name="input_vault_kms_key_alias_name"></a> [vault\_kms\_key\_alias\_name](#input\_vault\_kms\_key\_alias\_name) | Alias for the KMS key of the Backup vault. Ignored if `var.vault_kms_key_enabled` is `false`. Do not prefix this value with `alias/` nor with a `/`. | `string` | `"default/backupvault"` | no |
| <a name="input_vault_kms_key_arn"></a> [vault\_kms\_key\_arn](#input\_vault\_kms\_key\_arn) | ARN of the KMS key (or alias) to use for the vault for AWS Backup.<br>Expected to be used instead of the created KMS key, when `var.vault_kms_key_enabled` is `false`.<br>If `var.vault_kms_replica_key_enabled` is `true`, this ARN will be used as primary to create the replica key, in turn used instead of this ARN for the vault. | `string` | `null` | no |
| <a name="input_vault_kms_key_enable_key_rotation"></a> [vault\_kms\_key\_enable\_key\_rotation](#input\_vault\_kms\_key\_enable\_key\_rotation) | Specifies whether key rotation is enabled. Specifies whether key rotation is enabled. Defaults to `true`. | `bool` | `true` | no |
| <a name="input_vault_kms_key_enabled"></a> [vault\_kms\_key\_enabled](#input\_vault\_kms\_key\_enabled) | Whether or not to create a KMS key for the vault for AWS Backup. Ignored if `var.vault_enabled` is `false`. | `bool` | `true` | no |
| <a name="input_vault_kms_key_multi_region"></a> [vault\_kms\_key\_multi\_region](#input\_vault\_kms\_key\_multi\_region) | Indicates whether the KMS key for the vault for AWS Backup is a multi-Region (`true`) or regional (`false`). Ignored if `var.vault_kms_key_enabled` is `false`. | `any` | `null` | no |
| <a name="input_vault_kms_key_name"></a> [vault\_kms\_key\_name](#input\_vault\_kms\_key\_name) | Name of the KMS key to use for the vault for AWS Backup. Ignored if `var.vault_kms_key_enabled` is `false`. | `string` | `"vault-kms"` | no |
| <a name="input_vault_kms_key_policy"></a> [vault\_kms\_key\_policy](#input\_vault\_kms\_key\_policy) | A valid policy JSON document for the KMS key to use for the vault for AWS Backup. Ignored if `var.vault_kms_key_enabled` is `false`. | `any` | `null` | no |
| <a name="input_vault_kms_replica_key_enabled"></a> [vault\_kms\_replica\_key\_enabled](#input\_vault\_kms\_replica\_key\_enabled) | Whether or not to create a replica KMS key for the vault for AWS Backup. Ignored if `var.vault_enabled` is `false`. This key will have the highest priority to be used for the vault. | `bool` | `false` | no |
| <a name="input_vault_kms_tags"></a> [vault\_kms\_tags](#input\_vault\_kms\_tags) | Tags for the KMS key of the Backup vault. Will be merged with tags. | `map(string)` | `{}` | no |
| <a name="input_vault_lock_configuration_changeable_for_days"></a> [vault\_lock\_configuration\_changeable\_for\_days](#input\_vault\_lock\_configuration\_changeable\_for\_days) | The number of days before the lock date for the vault. Before the lock date, you can delete AWS Backup Vault Lock. Ignored if `var.vault_enabled` or `var.vault_lock_enabled` is `false`. | `number` | `3` | no |
| <a name="input_vault_lock_configuration_max_retention_days"></a> [vault\_lock\_configuration\_max\_retention\_days](#input\_vault\_lock\_configuration\_max\_retention\_days) | The maximum retention period that the vault retains its recovery points lock. If not specified, AWS Backup Vault Lock will not enforce a maximum retention period. Ignored if `var.vault_enabled` or `var.vault_lock_enabled` is `false`. | `number` | `null` | no |
| <a name="input_vault_lock_configuration_min_retention_days"></a> [vault\_lock\_configuration\_min\_retention\_days](#input\_vault\_lock\_configuration\_min\_retention\_days) | The minimum retention period that the vault retains its recovery points lock. If not specified, AWS Backup Vault Lock will not enforce a minimum retention period. Ignored if `var.vault_enabled` or `var.vault_lock_enabled` is `false`. | `number` | `null` | no |
| <a name="input_vault_lock_enabled"></a> [vault\_lock\_enabled](#input\_vault\_lock\_enabled) | Whether or not to setup a vault recovery point lock for AWS Backup. | `bool` | `false` | no |
| <a name="input_vault_name"></a> [vault\_name](#input\_vault\_name) | Name of the backup vault to use. If `var.vault_enabled` is `true`, this will be the name of the created vault. Otherwise, this variable should be the name of a vault created outside this module. | `string` | `"vault"` | no |
| <a name="input_vault_policy"></a> [vault\_policy](#input\_vault\_policy) | Policy to attach to the Backup Vault in JSON format. Ignored if `var.vault_enabled` is `false`. | `string` | `null` | no |
| <a name="input_vault_policy_enabled"></a> [vault\_policy\_enabled](#input\_vault\_policy\_enabled) | Whether or not to create a policy tight to the AWS Backup Vault. Only work when `var.vault_enabled` is `true`. | `bool` | `false` | no |
| <a name="input_vault_tags"></a> [vault\_tags](#input\_vault\_tags) | Tags for the Backup vault. Will be merged with `var.tags`. Ignored if `var.vault_kms_key_enabled` is `false`. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_plan_arns"></a> [plan\_arns](#output\_plan\_arns) | n/a |
| <a name="output_plan_versions"></a> [plan\_versions](#output\_plan\_versions) | n/a |
| <a name="output_selection_iam_role_arn"></a> [selection\_iam\_role\_arn](#output\_selection\_iam\_role\_arn) | n/a |
| <a name="output_selection_iam_role_name"></a> [selection\_iam\_role\_name](#output\_selection\_iam\_role\_name) | n/a |
| <a name="output_selection_iam_role_unique_id"></a> [selection\_iam\_role\_unique\_id](#output\_selection\_iam\_role\_unique\_id) | n/a |
| <a name="output_selection_resources_ids"></a> [selection\_resources\_ids](#output\_selection\_resources\_ids) | n/a |
| <a name="output_selection_tag_id"></a> [selection\_tag\_id](#output\_selection\_tag\_id) | n/a |
| <a name="output_vault_arn"></a> [vault\_arn](#output\_vault\_arn) | n/a |
| <a name="output_vault_id"></a> [vault\_id](#output\_vault\_id) | n/a |
| <a name="output_vault_kms_key_alias_arn"></a> [vault\_kms\_key\_alias\_arn](#output\_vault\_kms\_key\_alias\_arn) | n/a |
| <a name="output_vault_kms_key_arn"></a> [vault\_kms\_key\_arn](#output\_vault\_kms\_key\_arn) | n/a |
| <a name="output_vault_kms_key_id"></a> [vault\_kms\_key\_id](#output\_vault\_kms\_key\_id) | n/a |
| <a name="output_vault_kms_key_replica_arn"></a> [vault\_kms\_key\_replica\_arn](#output\_vault\_kms\_key\_replica\_arn) | n/a |
| <a name="output_vault_kms_key_replica_id"></a> [vault\_kms\_key\_replica\_id](#output\_vault\_kms\_key\_replica\_id) | n/a |
| <a name="output_vault_recovery_points"></a> [vault\_recovery\_points](#output\_vault\_recovery\_points) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
