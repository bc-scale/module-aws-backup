2.1.0
=====

* feat: allows to create a KMS key replica, for multi-region
* feat: adds lifecycle to "copy_action"
* fix: adds a required provider: aws
* test: sets test EBS size to 1GB

2.0.0
=====

* feat: allow to specify multiple tags for selection by tags
* feat: allow to use external policies to attach to the Selection Backup role
* feat: allow to lock the vault recovery points for a period of time, preventing any kind of deletion.
* feat: allow to add a policy to the AWS Backup vault.
* feat: add `var.global_settings_cross_account_enabled` setting to allow cross account backups
* feat: add `var.region_settings_resource_type_opt_in` setting to opt-in/opt-out to some services for backup
* refactor: (BREAKING) modernize practices: renames, for_eaches, resource validations, descriptions…
* refactor: (BREAKING) Automatically prefix KMS alias with `alias/`
* refactor: removes descriptions from outputs
* tech: removes AUTHORS file
* tech: empty Jenkinsfile
* tech: update .gitignore
* test: cleans examples
* test: merges external examples together
* maintenance: updates .pre-commit dependencies
* tech: (BREAKING) terraform 1+ upgrade
* tech: add LICENSE

1.0.2
=====

* chore: bump pre-commit hooks
* fix: Add `AWSBackupServiceRolePolicyForRestores` to IAM role in order to backup KMS protected dynamodb ressources

1.0.1
=====

* fix: Change pre-commit version of hooks and apply
* refactor: Remove quotes on keywords

1.0.0
=====

* breaking: terraform 0.12 upgrade & best practices

0.1.0
=====

* Initial commit
