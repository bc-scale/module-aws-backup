locals {
  tags = merge(
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-backup"
    },
    var.tags
  )
}

####
# Global settings
####

resource "aws_backup_global_settings" "this" {
  for_each = var.global_settings_cross_account_enabled ? { 0 = 0 } : {}

  global_settings = {
    "isCrossAccountBackupEnabled" = var.global_settings_cross_account_enabled
  }
}

resource "aws_backup_region_settings" "this" {
  for_each = var.region_settings_resource_type_opt_in

  resource_type_opt_in_preference = var.region_settings_resource_type_opt_in
}

####
# Vault
####

resource "aws_backup_vault" "this" {
  for_each = var.vault_enabled ? { 0 = 0 } : {}

  name        = var.vault_name
  kms_key_arn = try(aws_kms_replica_key.this_vault.0.arn, aws_kms_key.this_vault.0.arn, var.vault_kms_key_arn)

  tags = merge(
    local.tags,
    var.vault_tags,
    {
      Name = var.vault_name
    },
  )
}

resource "aws_backup_vault_policy" "this" {
  for_each = var.vault_enabled && var.vault_policy_enabled ? { 0 = 0 } : {}

  backup_vault_name = aws_backup_vault.this.0.name
  policy            = var.vault_policy
}

resource "aws_backup_vault_lock_configuration" "this" {
  for_each = var.vault_enabled && var.vault_lock_enabled ? { 0 = 0 } : {}

  backup_vault_name   = aws_backup_vault.this.0.name
  changeable_for_days = var.vault_lock_configuration_changeable_for_days
  max_retention_days  = var.vault_lock_configuration_max_retention_days
  min_retention_days  = var.vault_lock_configuration_min_retention_days
}

resource "aws_kms_key" "this_vault" {
  for_each = var.vault_enabled && var.vault_kms_key_enabled ? { 0 = 0 } : {}

  description = "KMS Key for “${var.vault_name}” backup vault encryption."

  policy              = var.vault_kms_key_policy
  enable_key_rotation = var.vault_kms_key_enable_key_rotation
  multi_region        = var.vault_kms_key_multi_region

  tags = merge(
    local.tags,
    var.vault_kms_tags,
    {
      Name = var.vault_kms_key_name
    },
  )
}

resource "aws_kms_replica_key" "this_vault" {
  for_each = var.vault_enabled && var.vault_kms_replica_key_enabled ? { 0 = 0 } : {}

  description = "KMS Key for “${var.vault_name}” backup vault encryption. Replica of “${try(aws_kms_key.this_vault.0.arn, var.vault_kms_key_arn)}”"

  policy          = var.vault_kms_key_policy
  primary_key_arn = try(aws_kms_key.this_vault.0.arn, var.vault_kms_key_arn)

  tags = merge(
    local.tags,
    var.vault_kms_tags,
    {
      Name = var.vault_kms_key_name
    },
  )
}

resource "aws_kms_alias" "this_vault" {
  for_each = var.vault_enabled && (var.vault_kms_key_enabled || var.vault_kms_replica_key_enabled) ? { 0 = 0 } : {}

  name          = format("alias/%s", var.vault_kms_key_alias_name)
  target_key_id = try(aws_kms_replica_key.this_vault.0.key_id, aws_kms_key.this_vault.0.key_id)
}

####
# Plan
####

resource "aws_backup_plan" "this" {
  for_each = var.plan_enabled ? { 0 = 0 } : {}

  name = var.plan_name

  dynamic "rule" {
    for_each = var.plan_rules

    content {
      rule_name                = rule.value.rule_name
      target_vault_name        = try(aws_backup_vault.this.0.name, var.vault_name)
      schedule                 = lookup(rule.value, "schedule", null)
      enable_continuous_backup = lookup(rule.value, "enable_continuous_backup", null)
      start_window             = lookup(rule.value, "start_window", null)
      completion_window        = lookup(rule.value, "completion_window", null)
      recovery_point_tags = merge(
        local.tags,
        {
          managed-by = "aws-backup"
        },
        lookup(rule.value, "recovery_point_tags", null)
      )

      dynamic "lifecycle" {
        for_each = lookup(rule.value, "lifecycle", null) != null ? { 0 = lookup(rule.value, "lifecycle", {}) } : {}

        content {
          cold_storage_after = lookup(lifecycle.value, "cold_storage_after", null)
          delete_after       = lookup(lifecycle.value, "delete_after", null)
        }
      }

      dynamic "copy_action" {
        for_each = lookup(rule.value, "copy_action", null) != null ? { 0 = lookup(rule.value, "copy_action", {}) } : {}

        content {
          destination_vault_arn = lookup(copy_action.value, "destination_vault_arn", null)

          dynamic "lifecycle" {
            for_each = lookup(copy_action.value, "lifecycle", null) != null ? { 0 = lookup(copy_action.value, "lifecycle", {}) } : {}

            content {
              cold_storage_after = lookup(lifecycle.value, "cold_storage_after", null)
              delete_after       = lookup(lifecycle.value, "delete_after", null)
            }
          }
        }
      }
    }
  }

  tags = merge(
    local.tags,
    var.plan_tags,
    {
      Name = var.plan_name
    },
  )
}

####
# Selection
####

locals {
  selection_enabled = var.selection_by_tag_enabled || var.selection_by_resources_enabled
}

data "aws_iam_policy_document" "this_selection_assume_role" {
  for_each = local.selection_enabled && var.selection_role_enabled ? { 0 = 0 } : {}

  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = ["backup.amazonaws.com"]
      type        = "Service"
    }
  }
}

resource "aws_iam_role" "this_selection" {
  for_each = local.selection_enabled && var.selection_role_enabled ? { 0 = 0 } : {}

  name               = var.selection_role_name
  assume_role_policy = data.aws_iam_policy_document.this_selection_assume_role.0.json
}

resource "aws_iam_role_policy_attachment" "this_selection_backup" {
  for_each = local.selection_enabled && var.selection_role_enabled ? { 0 = 0 } : {}

  policy_arn = var.selection_role_backup_policy_arn
  role       = aws_iam_role.this_selection.0.name
}

resource "aws_iam_role_policy_attachment" "this_selection_restore" {
  for_each = local.selection_enabled && var.selection_role_enabled ? { 0 = 0 } : {}

  policy_arn = var.selection_role_restore_policy_arn
  role       = aws_iam_role.this_selection.0.name
}

resource "aws_backup_selection" "by_tags" {
  for_each = var.selection_by_tag_enabled ? { 0 = 0 } : {}

  iam_role_arn = try(aws_iam_role.this_selection.0.arn, var.selection_role_arn)
  name         = var.selection_by_tag_name
  plan_id      = var.plan_enabled ? aws_backup_plan.this.0.id : var.plan_id

  dynamic "selection_tag" {
    for_each = var.selection_by_tag_tags

    content {
      type  = selection_tag.value.type
      key   = selection_tag.value.key
      value = selection_tag.value.value
    }
  }

  depends_on = [
    aws_iam_role_policy_attachment.this_selection_backup,
    aws_iam_role_policy_attachment.this_selection_restore
  ]
}

resource "aws_backup_selection" "by_resources" {
  for_each = var.selection_by_resources_enabled ? { 0 = 0 } : {}

  iam_role_arn = var.selection_role_enabled ? aws_iam_role.this_selection.0.arn : var.selection_role_arn
  name         = var.selection_by_resource_name
  plan_id      = var.plan_enabled ? aws_backup_plan.this.0.id : var.plan_id

  resources = var.selection_by_resource_resources

  depends_on = [
    aws_iam_role_policy_attachment.this_selection_backup,
    aws_iam_role_policy_attachment.this_selection_restore
  ]
}
