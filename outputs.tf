####
# Vault
####

output "vault_id" {
  value = try(aws_backup_vault.this.0.id, null)
}

output "vault_arn" {
  value = try(aws_backup_vault.this.0.arn, null)
}

output "vault_recovery_points" {
  value = try(aws_backup_vault.this.0.recovery_points, null)
}

output "vault_kms_key_arn" {
  value = try(aws_kms_key.this_vault.0.arn, null)
}

output "vault_kms_key_id" {
  value = try(aws_kms_key.this_vault.0.key_id, null)
}

output "vault_kms_key_alias_arn" {
  value = try(aws_kms_alias.this_vault.0.arn, null)
}

output "vault_kms_key_replica_arn" {
  value = try(aws_kms_replica_key.this_vault.0.arn, null)
}

output "vault_kms_key_replica_id" {
  value = try(aws_kms_replica_key.this_vault.0.key_id, null)
}

####
# Plan
####

output "plan_arns" {
  value = try(aws_backup_plan.this.0.arn, null)
}

output "plan_versions" {
  value = try(aws_backup_plan.this.0.version, null)
}

output "selection_iam_role_arn" {
  value = try(aws_iam_role.this_selection.0.arn, null)
}

output "selection_iam_role_name" {
  value = try(aws_iam_role.this_selection.0.name, null)
}

output "selection_iam_role_unique_id" {
  value = try(aws_iam_role.this_selection.0.unique_id, null)
}

####
# Selection
####

output "selection_tag_id" {
  value = try(aws_backup_selection.by_tags.0.id, null)
}

output "selection_resources_ids" {
  value = try(aws_backup_selection.by_resources.0.id, null)
}
