#####
# Module
#####

variable "tags" {
  description = "Tags to be shared among all resources of this module."
  default     = {}
  type        = map(string)
}

####
# Global settings
####

variable "global_settings_cross_account_enabled" {
  description = "Whether or not to allow cross account backups. This only works from the root account."
  default     = false
}

variable "region_settings_resource_type_opt_in" {
  description = "A map of services along with the opt-in preferences for the Region. Once opted-in, services stays opted-in, even if the resources are destroyed. Unmentioned services stays untouched. For values, see: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/backup_region_settings."
  default     = {}
  type        = map(bool)
}

####
# Vault
####

variable "vault_enabled" {
  description = "Whether or not to create a vault for AWS Backup."
  default     = true
}

variable "vault_name" {
  description = "Name of the backup vault to use. If `var.vault_enabled` is `true`, this will be the name of the created vault. Otherwise, this variable should be the name of a vault created outside this module."
  default     = "vault"

  validation {
    condition     = var.vault_name == null || can(regex("^[a-zA-Z0-9\\-\\_]{2,50}$", var.vault_name))
    error_message = "The var.vault_name must match “^[a-zA-Z0-9\\-\\_]{2,50}$”."
  }
}

variable "vault_policy_enabled" {
  description = "Whether or not to create a policy tight to the AWS Backup Vault. Only work when `var.vault_enabled` is `true`."
  default     = false
}

variable "vault_policy" {
  description = "Policy to attach to the Backup Vault in JSON format. Ignored if `var.vault_enabled` is `false`."
  default     = null
  type        = string

  validation {
    condition     = var.vault_policy == null || can(jsonencode(var.vault_policy))
    error_message = "The var.vault_policy must be a valid JSON document."
  }
}

variable "vault_lock_enabled" {
  description = "Whether or not to setup a vault recovery point lock for AWS Backup."
  default     = false
}

variable "vault_lock_configuration_changeable_for_days" {
  description = "The number of days before the lock date for the vault. Before the lock date, you can delete AWS Backup Vault Lock. Ignored if `var.vault_enabled` or `var.vault_lock_enabled` is `false`."
  default     = 3
  type        = number

  validation {
    condition     = var.vault_lock_configuration_changeable_for_days >= 3 && var.vault_lock_configuration_changeable_for_days <= 36500
    error_message = "The var.vault_lock_configuration_changeable_for_days must be between 3 and 36500."
  }
}

variable "vault_lock_configuration_max_retention_days" {
  description = "The maximum retention period that the vault retains its recovery points lock. If not specified, AWS Backup Vault Lock will not enforce a maximum retention period. Ignored if `var.vault_enabled` or `var.vault_lock_enabled` is `false`."
  default     = null
  type        = number

  validation {
    condition     = var.vault_lock_configuration_max_retention_days == null || (coalesce(var.vault_lock_configuration_max_retention_days, 1) >= 1 && coalesce(var.vault_lock_configuration_max_retention_days, 1) <= 36500)
    error_message = "The var.vault_lock_configuration_max_retention_days must be between 1 and 36500."
  }
}

variable "vault_lock_configuration_min_retention_days" {
  description = "The minimum retention period that the vault retains its recovery points lock. If not specified, AWS Backup Vault Lock will not enforce a minimum retention period. Ignored if `var.vault_enabled` or `var.vault_lock_enabled` is `false`."
  default     = null
  type        = number

  validation {
    condition     = var.vault_lock_configuration_min_retention_days == null || (coalesce(var.vault_lock_configuration_min_retention_days, 1) >= 1 && coalesce(var.vault_lock_configuration_min_retention_days, 1) <= 36500)
    error_message = "The var.vault_lock_configuration_min_retention_days must be between 1 and 36500."
  }
}

variable "vault_kms_key_arn" {
  description = <<-DOCUMENTATION
    ARN of the KMS key (or alias) to use for the vault for AWS Backup.
    Expected to be used instead of the created KMS key, when `var.vault_kms_key_enabled` is `false`.
    If `var.vault_kms_replica_key_enabled` is `true`, this ARN will be used as primary to create the replica key, in turn used instead of this ARN for the vault.
DOCUMENTATION
  default     = null
  type        = string

  validation {
    condition     = var.vault_kms_key_arn == null || can(regex("^arn:aws(-us-gov|-cn)?:kms:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:[0-9]{12}:(key/[a-z0-9-]{36}|alias/[a-zA-Z0-9\\/-]+)$", var.vault_kms_key_arn))
    error_message = "The var.vault_kms_key_arn must match “^arn:aws(-us-gov|-cn)?:kms:([a-z0-9-]{6,16})?:[0-9]{12}:(key/[a-z0-9-]{36}|alias/[a-zA-Z0-9\\/-]+)$”."
  }
}

variable "vault_kms_key_enabled" {
  description = "Whether or not to create a KMS key for the vault for AWS Backup. Ignored if `var.vault_enabled` is `false`."
  default     = true
}

variable "vault_kms_replica_key_enabled" {
  description = "Whether or not to create a replica KMS key for the vault for AWS Backup. Ignored if `var.vault_enabled` is `false`. This key will have the highest priority to be used for the vault."
  default     = false
}

variable "vault_kms_key_name" {
  description = "Name of the KMS key to use for the vault for AWS Backup. Ignored if `var.vault_kms_key_enabled` is `false`."
  default     = "vault-kms"

  validation {
    condition     = 1 <= length(var.vault_kms_key_name) && length(var.vault_kms_key_name) <= 128
    error_message = "The var.vault_kms_key_name length must be between 1 and 128."
  }
}

variable "vault_kms_key_policy" {
  description = "A valid policy JSON document for the KMS key to use for the vault for AWS Backup. Ignored if `var.vault_kms_key_enabled` is `false`."
  default     = null

  validation {
    condition     = var.vault_kms_key_policy != null ? (can(jsondecode(var.vault_kms_key_policy)) && length(var.vault_kms_key_policy) < 131072) : true
    error_message = "The var.volume_kms_key_policy must be a valid JSON string that does not exceed 131072 characters."
  }
}

variable "vault_kms_key_enable_key_rotation" {
  description = "Specifies whether key rotation is enabled. Specifies whether key rotation is enabled. Defaults to `true`."
  default     = true
}

variable "vault_kms_key_multi_region" {
  description = "Indicates whether the KMS key for the vault for AWS Backup is a multi-Region (`true`) or regional (`false`). Ignored if `var.vault_kms_key_enabled` is `false`."
  default     = null
}

variable "vault_kms_key_alias_name" {
  description = "Alias for the KMS key of the Backup vault. Ignored if `var.vault_kms_key_enabled` is `false`. Do not prefix this value with `alias/` nor with a `/`."
  default     = "default/backupvault"

  validation {
    condition     = can(regex("^[a-zA-Z0-9/_-]{1,256}$", var.vault_kms_key_alias_name)) && !can(regex("^(alias\\/|\\/).*", var.vault_kms_key_alias_name))
    error_message = "The var.vault_kms_key_alias_name must match “^[a-zA-Z0-9/_-]{1,256}$” and must not match “^(alias\\/|\\/).*”."
  }
}

variable "vault_tags" {
  description = "Tags for the Backup vault. Will be merged with `var.tags`. Ignored if `var.vault_kms_key_enabled` is `false`."
  default     = {}
  type        = map(string)
}

variable "vault_kms_tags" {
  description = "Tags for the KMS key of the Backup vault. Will be merged with tags."
  default     = {}
  type        = map(string)
}

####
# Plan
####

variable "plan_enabled" {
  description = "Whether or not to create a plan."
  default     = true
}

variable "plan_name" {
  description = "Name of the backup plan."
  default     = "backup-plan"

  validation {
    condition     = var.plan_name == null || can(regex("^[a-zA-Z0-9\\-\\_\\.]{1,50}$", var.plan_name))
    error_message = "The var.plan_name must match “^[a-zA-Z0-9\\-\\_\\.]{1,50}$”."
  }
}

variable "plan_id" {
  description = "The backup plan ID to be associated with the Backup Selections (by tag or by resources) of resources. Must be specified if `var.plan_enabled` is `false` and any of the Backup Selections are enabled."
  default     = ""
}

variable "plan_rules" {
  description = <<-DOCUMENTATION
    A rule object that specifies a scheduled task that is used to back up a selection of resources.
      * rule_name                (required, string): An display name for a backup rule.
      * schedule                 (required, string): A CRON expression specifying when AWS Backup initiates a backup job. See: https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html?icmpid=docs_console_unmapped
      * enable_continuous_backup (optional, bool): Enable continuous backups for supported resources.
      * start_window             (optional, string): The amount of time in minutes before beginning a backup.
      * completion_window        (optional, string): The amount of time AWS Backup attempts a backup before canceling the job and returning an error.
      * lifecycle                (optional, object): The lifecycle defines when a protected resource is transitioned to cold storage and when it expires. Fields documented below.
        * cold_storage_after (optional, number): Specifies the number of days after creation that a recovery point is moved to cold storage.
        * delete_after       (optional, number): Specifies the number of days after creation that a recovery point is deleted. Must be 90 days greater than `cold_storage_after`.
      * copy_action              (optional, object): Copy operation settings.
        * lifecycle             (optional, object): See `lifecycle`.
        * destination_vault_arn (optional, string): An Amazon Resource Name (ARN) that uniquely identifies the destination backup vault for the copied backup.
      * recovery_point_tags        (optional, map(string)): Metadata that you can assign to help organize the resources that you create. Will be merged with `var.tags`.
DOCUMENTATION
  type = list(object({
    rule_name                = string
    schedule                 = string
    enable_continuous_backup = optional(bool)
    start_window             = optional(string)
    completion_window        = optional(string)
    lifecycle = optional(object({
      cold_storage_after = optional(number)
      delete_after       = optional(number)
    }))
    recovery_point_tags = optional(map(string))
    copy_action = optional(object({
      lifecycle = optional(object({
        cold_storage_after = optional(number)
        delete_after       = optional(number)
      }))
      destination_vault_arn = string
    }))
  }))
  default = []

  validation {
    condition = !contains([
      for k, plan_rule in coalesce(var.plan_rules, []) :
      (
        can(regex("^[a-zA-Z0-9\\-\\_\\.]{1,50}$", plan_rule.rule_name)) &&
        lookup(plan_rule, "schedule", null) != null ? can(regex("^cron\\([,*\\-\\?0-9]{1,2} [,*\\-\\?0-9]{1,2} [,*\\-\\?0-9]{1,2} .{1,3} .{1,3} [,*\\-0-9]{1,4}\\)$", plan_rule.schedule)) : true &&
        (
          defaults(plan_rule, { start_window = 60 }).start_window <= 10080 || defaults(plan_rule, { start_window = 60 }).start_window >= 60
        ) &&
        (
          defaults(plan_rule, { completion_window = 60 }).completion_window <= 10080 || defaults(plan_rule, { completion_window = 60 }).completion_window >= 60
        )
      )
    ], false)
    error_message = "One or more “var.plan_rules” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "plan_tags" {
  description = "Tags for the Backup plan. Will be merged with tags."
  default     = {}
  type        = map(string)
}

####
# Selection
####

variable "selection_by_tag_enabled" {
  description = "Whether or not to create a Backup Selection by tags."
  default     = false
}

variable "selection_by_tag_name" {
  description = "Name of the selection by tags. Ignored if `var.selection_by_tags_enabled` is `false`."
  default     = "selection-by-tag"
}

variable "selection_by_tag_tags" {
  description = <<-DOCUMENTATION
    Tag-based conditions used to specify a set of resources to assign to a backup plan.
      * type  (required, string): An operation, such as `StringEquals`, that is applied to a key-value pair used to filter resources in a selection.
      * key   (required, string): The key in a key-value pair.
      * value (required, string): The value in a key-value pair.
DOCUMENTATION
  type = list(object({
    type  = string
    key   = string
    value = string
  }))
  default = []

  validation {
    condition = var.selection_by_tag_tags == [] || (
      (
        !contains([
          for k, content in var.selection_by_tag_tags :
          (
            contains(["STRINGEQUALS"], lookup(content, "type", "STRINGEQUALS"))
          )
        ], false)
      )
    )
    error_message = "One or more “var.selection_by_tag_tags” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "selection_by_resources_enabled" {
  description = "Whether or not to create a Backup Selection by resources."
  default     = false
}

variable "selection_by_resource_name" {
  description = "Name of the selection by resource. Ignored if `var.selection_by_resources_enabled` is `false`."
  default     = "selection-by-resource"
}

variable "selection_by_resource_resources" {
  description = "An array of strings that either contain Amazon Resource Names (ARNs) or match patterns of resources to assign to a backup plan. Ignored if `var.selection_by_resources_enabled` is `false`."
  default     = []

  validation {
    condition = !contains([
      for k, content in var.selection_by_resource_resources :
      (
        can(regex("^arn:aws(-us-gov|-cn)?:[a-z0-9]+:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws)?:[a-zA-Z0-9_\\/\\-]+$", content))
      )
    ], false)
    error_message = "One or more “var.selection_by_resource_resources” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "selection_role_enabled" {
  description = "Whether or not to create a role that AWS Backup uses to authenticate when restoring and backing up the target resources. Ignored if any selection is `false`."
  default     = true
}

variable "selection_role_name" {
  description = "Name of the IAM role that AWS Backup uses to authenticate when restoring and backing up the target resources. Ignored if `selection_role_enabled` is `false`."
  default     = null

  validation {
    condition     = var.selection_role_name == null || can(regex("^[_+=,\\.@a-zA-Z0-9-]{1,128}$", var.selection_role_name))
    error_message = "The var.selection_role_name must match “^[_+=,\\.@a-zA-Z0-9-]{1,128}$”."
  }
}

variable "selection_role_arn" {
  description = "ARN of the IAM role that AWS Backup uses to authenticate when restoring and backing up the target resource. Must be specified if `selection_role_enabled` is `false` and any Backup Selection is enabled."
  default     = null

  validation {
    condition     = var.selection_role_arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws)?:role/[_+=,\\.@a-zA-Z0-9-]{1,128}$", var.selection_role_arn))
    error_message = "The var.selection_role_arn must match “^arn:aws(-us-gov|-cn)?:iam:([a-z0-9-]{6,16})?:([0-9]{12}|aws)?:role/[_+=,\\.@a-zA-Z0-9-]{1,128}$”."
  }
}

variable "selection_role_backup_policy_arn" {
  description = "ARN of the policy to attach to the Backup Selection role that AWS Backup uses to authenticate when backing up the target resources. Defaults to the service policy."
  default     = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"

  validation {
    condition     = var.selection_role_backup_policy_arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws)?:policy/[a-zA-Z0-9_/-]+$", var.selection_role_backup_policy_arn))
    error_message = "The var.selection_role_backup_policy_arn must match “^arn:aws(-us-gov|-cn)?:iam:([a-z0-9-]{6,16})?:([0-9]{12}|aws)?:policy/[a-zA-Z0-9_/-]+$”."
  }
}

variable "selection_role_restore_policy_arn" {
  description = "ARN of the policy to attach to the Backup Selection role that AWS Backup uses to authenticate when restoring the target resources. Defaults to the service policy."
  default     = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForRestores"

  validation {
    condition     = var.selection_role_restore_policy_arn == null || can(regex("^arn:aws(-us-gov|-cn)?:iam:([a-z]{2}-[a-z]{4,10}-[1-9]{1})?:([0-9]{12}|aws)?:policy/[a-zA-Z0-9_/-]+$", var.selection_role_restore_policy_arn))
    error_message = "The var.selection_role_restore_policy_arn must match “^arn:aws(-us-gov|-cn)?:iam:([a-z0-9-]{6,16})?:([0-9]{12}|aws)?:policy/[a-zA-Z0-9_/-]+$”."
  }
}
