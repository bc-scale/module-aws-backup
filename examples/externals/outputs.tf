####
# External KMS
####

output "external_kms_vault_id" {
  value = module.external_kms.vault_id
}

output "external_kms_vault_arn" {
  value = module.external_kms.vault_arn
}

output "external_kms_vault_recovery_points" {
  value = module.external_kms.vault_recovery_points
}

output "external_kms_vault_kms_key_arn" {
  value = module.external_kms.vault_kms_key_arn
}

output "external_kms_vault_kms_key_id" {
  value = module.external_kms.vault_kms_key_id
}

output "external_kms_vault_kms_key_alias_arn" {
  value = module.external_kms.vault_kms_key_alias_arn
}

output "external_kms_vault_kms_key_replica_arn" {
  value = module.external_kms.vault_kms_key_replica_arn
}

output "external_kms_vault_kms_key_replica_id" {
  value = module.external_kms.vault_kms_key_replica_id
}

output "external_kms_plan_arns" {
  value = module.external_kms.plan_arns
}

output "external_kms_plan_versions" {
  value = module.external_kms.plan_versions
}

output "external_kms_selection_iam_role_arn" {
  value = module.external_kms.selection_iam_role_arn
}

output "external_kms_selection_iam_role_name" {
  value = module.external_kms.selection_iam_role_name
}

output "external_kms_selection_iam_role_unique_id" {
  value = module.external_kms.selection_iam_role_unique_id
}

output "external_kms_selection_tag_id" {
  value = module.external_kms.selection_tag_id
}

output "external_kms_selection_resources_ids" {
  value = module.external_kms.selection_resources_ids
}

####
# External Vault
####

output "external_vault_vault_id" {
  value = module.external_vault.vault_id
}

output "external_vault_vault_arn" {
  value = module.external_vault.vault_arn
}

output "external_vault_vault_recovery_points" {
  value = module.external_vault.vault_recovery_points
}

output "external_vault_vault_kms_key_arn" {
  value = module.external_vault.vault_kms_key_arn
}

output "external_vault_vault_kms_key_id" {
  value = module.external_vault.vault_kms_key_id
}

output "external_vault_vault_kms_key_alias_arn" {
  value = module.external_vault.vault_kms_key_alias_arn
}

output "external_vault_kms_key_replica_arn" {
  value = module.external_vault.vault_kms_key_replica_arn
}

output "external_vault_kms_key_replica_id" {
  value = module.external_vault.vault_kms_key_replica_id
}

output "external_vault_plan_arns" {
  value = module.external_vault.plan_arns
}

output "external_vault_plan_versions" {
  value = module.external_vault.plan_versions
}

output "external_vault_selection_iam_role_arn" {
  value = module.external_vault.selection_iam_role_arn
}

output "external_vault_selection_iam_role_name" {
  value = module.external_vault.selection_iam_role_name
}

output "external_vault_selection_iam_role_unique_id" {
  value = module.external_vault.selection_iam_role_unique_id
}

output "external_vault_selection_tag_id" {
  value = module.external_vault.selection_tag_id
}

output "external_vault_selection_resources_ids" {
  value = module.external_vault.selection_resources_ids
}
