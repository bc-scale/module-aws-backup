resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

resource "aws_kms_key" "example" {
}

resource "aws_backup_vault" "example" {
  name        = "tftest${random_string.this.result}Vault"
  kms_key_arn = aws_kms_key.example.arn
}

resource "aws_ebs_volume" "example" {
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "tftest"
  }
}

data "aws_iam_policy_document" "example" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    principals {
      identifiers = ["backup.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "example2" {
  statement {
    effect = "Allow"

    actions = [
      "sts:AssumeRole",
    ]

    resources = ["*"]
  }
}

resource "aws_iam_policy" "example" {
  name   = "tftest${random_string.this.result}Policy"
  policy = data.aws_iam_policy_document.example2.json
}

resource "aws_iam_role" "example" {
  name_prefix        = "tftest${random_string.this.result}Role"
  assume_role_policy = data.aws_iam_policy_document.example.json
}

####
# - external KMS key for vault
# - external Role for Backup Selection
####

module "external_kms" {
  source = "../../"

  tags = {
    tftest = "external_kms"
  }

  vault_enabled = true
  vault_name    = "tftest${random_string.this.result}ExternalKMSVault"
  vault_tags = {
    Name = "tftest"
  }
  vault_kms_key_enabled = false
  vault_kms_key_arn     = aws_kms_key.example.arn

  plan_enabled = true
  plan_name    = "tftest${random_string.this.result}ExternalKMSPlan"
  plan_tags = {
    Name = "tftest"
  }
  plan_rules = [
    {
      rule_name = "tftest${random_string.this.result}ExternalKMSRule"
      schedule  = "cron(0 6 1 * ? *)"
      recovery_point_tags = {
        Name = "tftest-plan-rule"
      }
    }
  ]

  selection_role_enabled         = false
  selection_role_arn             = aws_iam_role.example.arn
  selection_by_resources_enabled = true
  selection_by_resource_name     = "tftest${random_string.this.result}SelectionResource"
  selection_by_resource_resources = [
    aws_ebs_volume.example.arn,
  ]
}

####
# - external vault
# - overridden role policy
####

module "external_vault" {
  source = "../../"

  tags = {
    tftest = "external_vault"
  }

  vault_enabled = false
  vault_name    = aws_backup_vault.example.name

  plan_enabled = true
  plan_name    = "tftest${random_string.this.result}ExtVaultPlan"
  plan_rules = [
    {
      rule_name = "tftest${random_string.this.result}ExtVaultRule"
      schedule  = "cron(0 6 1 * ? *)"
      lifecycle = {
        cold_storage_after = 60
        delete_after       = 150
      }
      recovery_point_tags = {
        Name = "tftest-plan-rule"
      }
    }
  ]

  selection_role_enabled            = true
  selection_role_name               = "tftest${random_string.this.result}Role"
  selection_role_backup_policy_arn  = "arn:aws:iam::aws:policy/AWSBackupFullAccess"
  selection_role_restore_policy_arn = aws_iam_policy.example.arn

  selection_by_tag_enabled = true
  selection_by_tag_name    = "tftest${random_string.this.result}SelectionTag"
  selection_by_tag_tags = [
    {
      type  = "STRINGEQUALS"
      key   = "to-backup"
      value = "yes"
    }
  ]
}
