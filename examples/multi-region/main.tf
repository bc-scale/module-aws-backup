resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

data "aws_region" "current" {}
data "aws_region" "alternative" {
  provider = aws.us2
}

data "aws_iam_policy_document" "example" {
  statement {
    sid = "default"

    effect = "Allow"

    actions = [
      "backup:DescribeBackupVault",
      "backup:DeleteBackupVault",
      "backup:PutBackupVaultAccessPolicy",
      "backup:DeleteBackupVaultAccessPolicy",
      "backup:GetBackupVaultAccessPolicy",
      "backup:StartBackupJob",
      "backup:GetBackupVaultNotifications",
      "backup:PutBackupVaultNotifications",
    ]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [
      format("arn:aws:backup:%s::backup-vault/%s", data.aws_region.current.name, "tftest${random_string.this.result}MultiRegionVault")
    ]
  }
}

data "aws_iam_policy_document" "example_alternative" {
  statement {
    sid = "default"

    effect = "Allow"

    actions = [
      "backup:DescribeBackupVault",
      "backup:DeleteBackupVault",
      "backup:PutBackupVaultAccessPolicy",
      "backup:DeleteBackupVaultAccessPolicy",
      "backup:GetBackupVaultAccessPolicy",
      "backup:StartBackupJob",
      "backup:GetBackupVaultNotifications",
      "backup:PutBackupVaultNotifications",
    ]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [
      format("arn:aws:backup:%s::backup-vault/%s", data.aws_region.alternative.name, "tftest${random_string.this.result}MultiRegionVault")
    ]
  }
}

module "multi_region1" {
  source = "../../"

  tags = {
    tftest = "multi-region1"
  }

  vault_enabled        = true
  vault_name           = "tftest${random_string.this.result}MultiRegionVault"
  vault_policy_enabled = true
  vault_policy         = data.aws_iam_policy_document.example.json
  vault_tags = {
    tftest = "multi-region1"
  }

  vault_kms_key_enabled         = false
  vault_kms_replica_key_enabled = true
  vault_kms_key_arn             = module.multi_region2.vault_kms_key_arn
  vault_kms_key_alias_name      = "tftest/${random_string.this.result}"
  vault_kms_tags = {
    tftest = "multi-region1"
  }

  plan_enabled = true
  plan_name    = "tftest${random_string.this.result}MultiRegionVaultPlan"
  plan_rules = [
    {
      rule_name = "tftest${random_string.this.result}Region1Rule"
      schedule  = "cron(0 6 1 * ? *)"
      copy_action = {
        destination_vault_arn = module.multi_region2.vault_arn
      }
    }
  ]
}

module "multi_region2" {
  source = "../../"

  tags = {
    tftest = "multi-region2"
  }

  vault_enabled        = true
  vault_name           = "tftest${random_string.this.result}MultiRegionVault"
  vault_policy_enabled = true
  vault_policy         = data.aws_iam_policy_document.example_alternative.json
  vault_tags = {
    tftest = "multi-region2"
  }

  vault_kms_key_name         = "tftest${random_string.this.result}KMSKeyName"
  vault_kms_key_multi_region = true
  vault_kms_key_alias_name   = "tftest/${random_string.this.result}"
  vault_kms_tags = {
    tftest = "multi-region2"
  }

  plan_enabled = false

  providers = {
    aws = aws.us2
  }
}
