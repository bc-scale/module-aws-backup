resource "random_string" "this" {
  length  = 8
  upper   = false
  special = false
}

resource "aws_ebs_volume" "example" {
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "tftest"
  }
}

data "aws_iam_policy_document" "example" {
  statement {
    sid = "default"

    effect = "Allow"

    actions = [
      "backup:DescribeBackupVault",
      "backup:DeleteBackupVault",
      "backup:PutBackupVaultAccessPolicy",
      "backup:DeleteBackupVaultAccessPolicy",
      "backup:GetBackupVaultAccessPolicy",
      "backup:StartBackupJob",
      "backup:GetBackupVaultNotifications",
      "backup:PutBackupVaultNotifications",
    ]

    principals {
      identifiers = ["*"]
      type        = "AWS"
    }

    resources = [
      format("arn:aws:backup:us-east-1::backup-vault/%s", "tftest${random_string.this.result}Vault")
    ]
  }
}

module "default" {
  source = "../../"

  tags = {
    Name = "toBeOverriden"
  }

  vault_enabled            = true
  vault_name               = "tftest${random_string.this.result}Vault"
  vault_kms_key_name       = "tftest${random_string.this.result}KMSKeyName"
  vault_kms_key_alias_name = "tftest/${random_string.this.result}"
  vault_policy_enabled     = true
  vault_policy             = data.aws_iam_policy_document.example.json
  vault_tags = {
    Name = "tftest"
  }
  vault_kms_tags = {
    Name = "tftest"
  }

  vault_lock_enabled                           = true
  vault_lock_configuration_changeable_for_days = 15
  vault_lock_configuration_max_retention_days  = 5
  vault_lock_configuration_min_retention_days  = 1

  plan_enabled = true
  plan_name    = "tftest${random_string.this.result}Plan"
  plan_rules = [
    {
      rule_name         = "tftest${random_string.this.result}Rule"
      schedule          = "cron(0 12 * * ? *)"
      start_window      = "60"
      completion_window = "120"
      recovery_point_tags = {
        Name = "tftest-plan-rule1"
      }
    },
    {
      rule_name = "tftest${random_string.this.result}Rule2"
      schedule  = "cron(0 5 ? * * *)"
      lifecycle = {
        cold_storage_after = 30
        delete_after       = 365
      }
      recovery_point_tags = {
        Name = "tftest-plan-rule2"
      }
    }
  ]
  plan_tags = {
    Name = "tftest"
  }

  selection_role_enabled = true
  selection_role_name    = "tftest${random_string.this.result}Role"

  selection_by_tag_enabled = true
  selection_by_tag_name    = "tftest${random_string.this.result}SelectionTag"
  selection_by_tag_tags = [
    {
      type  = "STRINGEQUALS"
      key   = "Backup"
      value = "true"
    },
    {
      type  = "STRINGEQUALS"
      key   = "Backup"
      value = "1"
    },
  ]

  selection_by_resources_enabled = true
  selection_by_resource_name     = "tftest${random_string.this.result}SelectionResource"
  selection_by_resource_resources = [
    aws_ebs_volume.example.arn,
  ]
}
